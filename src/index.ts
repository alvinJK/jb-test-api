import * as Hapi from "@hapi/hapi";

import db from "./db";
import routes from "./routes";
import getDate from "./plugins/getDate";

const init = async () => {
  /*
  reminder:
  Using commands from the docs `Hapi.server({...})` got errors from typescript
  https://github.com/DefinitelyTyped/DefinitelyTyped/issues/32994#issuecomment-462877289
  */
  const server = new Hapi.Server({
    port: 3030,
    host: "localhost",
    routes: {
      cors: true,
    },
  });

  db.authenticate()
    .then(() => {
      console.log("Connection has been established successfully.");
      db.sync().then(() => {
        console.log("synced");
      });
    })
    .catch((err: Error) => {
      console.error("Unable to connect to the database:", err);
    });

  server.route(routes);
  await server.register([
    {
      plugin: getDate,
      options: {
        format: "yyyy-mm-dd",
      },
    },
  ]);

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
