import { Sequelize } from "sequelize";

const db = new Sequelize("productmanager", "tester", "123456", {
  host: "localhost",
  dialect: "postgres",
});

export default db;
