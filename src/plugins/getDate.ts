import * as Hapi from "@hapi/hapi";

const getDate = {
  name: "getDate",
  version: "1.0.0",
  register: async function (server: Hapi.Server, options: { format?: string }) {
    const currentDate = function () {
      const date = new Date();
      const { format } = options;
      if (format && format === "yyyy-mm-dd") {
        return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
      }
      return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    };
    server.decorate("toolkit", "getDate", currentDate);
  },
};

export default getDate;
