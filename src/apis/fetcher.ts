import axios from "axios";

const fetcher = axios.create({
  baseURL: "http://api.elevenia.co.id/rest",
  headers: {
    openapikey: "721407f393e84a28593374cc2b347a98",
  },
});
export default fetcher;
