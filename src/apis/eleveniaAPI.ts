import fetcher from "./fetcher";
import { AxiosInstance, AxiosRequestConfig } from "axios";

export function createEleveniaAPI(fetch: AxiosInstance) {
  return {
    fetchProducts: async () => {
      const options: AxiosRequestConfig = {
        method: "GET",
      };
      // ?page={pageNumber}
      const response = await fetch("/prodservices/product/listing", options);
      return response.data;
    },
    fetchProductDetail: async (productNo: string) => {
      const options: AxiosRequestConfig = {
        method: "GET",
      };
      const response = await fetch(
        `/prodservices/product/details/${productNo}`,
        options
      );
      return response.data;
    },
  };
}

export default createEleveniaAPI(fetcher);
