import generalRoutes from "./general";
import productRoutes from "./products";

export default [...generalRoutes, ...productRoutes];
