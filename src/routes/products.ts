import { Request, ResponseToolkit } from "@hapi/hapi";
import { parse } from "fast-xml-parser";

import eleveniaAPI from "../apis/eleveniaAPI";
import Product from "../models/products.model";

type PatchRequest = Request & {
  payload: {
    productName: string;
    price: number;
    desc: string;
    image: string;
  };
};

const fetchProductsFromElevenia = {
  method: "GET",
  path: "/elevenia/products",
  handler: async (_request: Request, _h: ResponseToolkit) => {
    try {
      const data = await eleveniaAPI.fetchProducts();

      const jsonResult = parse(data);
      const { product } = jsonResult.Products;

      const arrNo = product.map((prod: { prdNo: number }) => prod.prdNo);
      const existingProducts: Array<Product> = await Product.findAll({
        where: { productNo: arrNo },
      });
      const arrExist = existingProducts.map((prod) => prod.productNo);

      product.forEach((prod: { prdNo: number }) => {
        const { prdNo } = prod;
        if (!arrExist.includes(prdNo)) {
          eleveniaAPI
            .fetchProductDetail(String(prdNo))
            .then((detailData) => {
              const detailResult: {
                Product: {
                  prdNm: string;
                  prdNo: number;
                  selPrc: number;
                  htmlDetail: string;
                  prdImage01: string;
                };
              } = parse(detailData);
              const {
                prdNm: productName,
                prdNo: productNo,
                selPrc: price,
                htmlDetail: desc,
                prdImage01: image,
              } = detailResult.Product;
              Product.create({
                productNo,
                productName,
                desc: desc.substr(500),
                price,
                image,
              }).catch((err) => {
                console.error(err);
              });
            })
            .catch((err) => {
              console.error(err);
            });
        }
      });

      return {
        status: "--",
        message: "Product Fetched and will be processed",
      };
    } catch (error) {
      console.log(error);
    }
    return {
      status: "XX",
      message: "Product Fetch fails",
    };
  },
};

const getProducts = {
  method: "GET",
  path: "/products",
  handler: async (_request: Request, _h: ResponseToolkit) => {
    try {
      const result = await Product.findAll({
        order: [["productNo", "ASC"]],
      });
      return {
        status: "--",
        message: "",
        data: result,
      };
    } catch (error) {
      console.error(error);
    }
    return {
      status: "XX",
      message: "Product Fetch fails",
      data: [],
    };
  },
};
const getProductDetail = {
  method: "GET",
  path: "/products/{id}",
  handler: async (request: Request, _h: ResponseToolkit) => {
    const { id } = request.params;
    try {
      const result = await Product.findOne({ where: { productNo: id } });
      return {
        status: "--",
        message: "",
        data: result,
      };
    } catch (error) {
      console.error(error);
    }
    return {
      status: "XX",
      message: "Product Detail Fetch fails",
      data: {},
    };
  },
};

const updateProductDetail = {
  method: "PATCH",
  path: "/products/{id}",
  handler: async (request: PatchRequest, _h: ResponseToolkit) => {
    const { id } = request.params;
    const { productName, desc, price, image } = request.payload;
    try {
      await Product.update(
        {
          productName,
          desc,
          price,
          image,
        },
        { where: { productNo: id } }
      );
      return {
        status: "--",
        message: "Success",
      };
    } catch (error) {
      console.error(error);
    }
    return {
      status: "XX",
      message: "Update product fails",
    };
  },
};

const deleteProduct = {
  method: "DELETE",
  path: "/products/{id}",
  handler: async (request: Request, _h: ResponseToolkit) => {
    const { id } = request.params;
    try {
      await Product.destroy({ where: { productNo: id } });
      return {
        status: "--",
        message: "Success",
      };
    } catch (error) {
      console.error(error);
    }
    return {
      status: "XX",
      message: "Delete product fails",
    };
  },
};

export default [
  fetchProductsFromElevenia,
  getProducts,
  getProductDetail,
  updateProductDetail,
  deleteProduct,
];
