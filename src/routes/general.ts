import { Request, ResponseToolkit } from "@hapi/hapi";

const getHomePage = {
  method: "GET",
  path: "/",
  handler: (
    _request: Request,
    h: ResponseToolkit & { getDate: () => string }
  ) => {
    return `Hi, welcome to the hapi api project. Current Date: ${h.getDate()}`;
  },
};

const getAboutPage = {
  method: "GET",
  path: "/about",
  handler: (_request: Request, _h: ResponseToolkit) => {
    return {
      errorCode: "00",
      message: "Auto JSON parsing without body-parser is pretty cool bois.",
    };
  },
};

export default [getHomePage, getAboutPage];
