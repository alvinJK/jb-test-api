import { Model, DataTypes } from "sequelize";
import db from "../db";

class Products extends Model {
  public productNo: number;
  public productName: string;
  public desc: string;
  public price: number;
  public image: string;
}

Products.init(
  {
    productNo: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    productName: {
      type: new DataTypes.STRING(100),
      allowNull: false,
    },
    desc: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    image: {
      type: new DataTypes.STRING(200),
      allowNull: false,
    },
  },
  {
    sequelize: db,
    tableName: "products",
  }
);

export default Products;
